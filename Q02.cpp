#include<stdio.h> 

int fibonacci(int num)
{    
    if(num <= 1)
    {
        return num;
    }
    else
    {
        return fibonacci(num-1) + fibonacci(num-2);
    }
}
int main()
{    
    int value;
    printf(" n =  ");
    scanf("%d", &value);       
    for(int n = 0; n <= value; n++)
    {
        printf("  %d \n ", fibonacci(n));
        printf("\n");
    }
    return 0; 
} 
